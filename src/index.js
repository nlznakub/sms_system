const { isValidMobilePhoneNumber } = require('./validate_mobile');
const request = require('request-promise')

class smsClass {
	constructor(username = '', password = '', sid = '', smsType = 'mailbit') {
		this.username = username;
		this.password = password;
		this.smsType = smsType;
		this.sid = sid
	}

	checkMobileNoFormat(mobileNo) {
		return isValidMobilePhoneNumber(mobileNo)
	}

	/**
	 * 
	 * @param {number} mobileNo 
	 * @param {string} msg 
	 */
	async sendSMS(mobileNo, msg = '') {
		mobileNo = this.checkMobileNoFormat(mobileNo);
		if (!mobileNo) {
			return { success: false, message: 'หมายเลขเบอร์โทรศัพท์ไม่ถูกต้อง' }
		}

		try {
			const { smsType } = this;
			let errCode, errMessage;
			if (smsType === 'twilio') {
				let { errorCode, errorMessage } = await this.sendByTwilio(mobileNo, msg);
				errCode = errorCode;
				errMessage = errorMessage;
			} else {
				let { errorCode, errorMessage } = await this.sendByMailbit(mobileNo, msg);
				errCode = errorCode;
				errMessage = errorMessage;
			}

			if (errCode) {
				return { success: false, message: 'ส่ง SMS ไม่สำเร็จ', message_error: JSON.stringify(errMessage) };
			}

			return { success: true, message: 'ส่ง SMS สำเร็จ', message_send: msg };
		} catch (err) {
			console.log('Error', err)
			return { success: false, message: 'ส่ง SMS ไม่สำเร็จ', message_error: JSON.stringify(err) };
		}
	};

	/**
	 * 
	 * @param {number} length = 6
	 */
	generateOTP(length = 6) {
		const _sym = '1234567890';
		const count = length;
		let str = '';

		for (let index = 0; index < count; index++) {
			str += _sym[parseInt(Math.random() * (_sym.length))];
		}
		return str;
	}

	async sendByTwilio(mobileNo, msg) {
		const { username, password } = this;
		const client = require('twilio')(username, password);
		let { errorCode, errorMessage } = await client.messages.create({
			body: msg,
			from: '+12562578759',
			to: mobileNo.replace('0', '+66')
		})
		return { errorCode, errorMessage }
	}

	async sendByMailbit(mobileNo, msg) {
		const USER = this.username;
		const PASSWORD = this.password;
		const SID = this.sid;

		let res = await request({
			method: 'POST',
			uri: `http://sms.mailbit.co.th/vendorsms/pushsms.aspx`,
			qs: {
				user: USER, // dplusfocus
				password: PASSWORD, // focus123
				msisdn: mobileNo.replace('0', '66'),
				sid: SID, // Focus
				msg,
				fl: 0,
				dc: 8
			}
		});
		let { ErrorCode, ErrorMessage } = JSON.parse(res)
		if (ErrorMessage.toLowerCase() === 'success') {
			return { errorCode: null };
		} else {
			return { errorCode: ErrorCode, errorMessage: ErrorMessage };
		}
	}
}

module.exports = smsClass;

