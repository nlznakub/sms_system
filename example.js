const smsClass = require('./src');
const stdin = process.openStdin();
console.log("Input: Phone_number, Message");

stdin.addListener("data", async (d) => {
    const split = d.toString().trim().split(',');
    const telephone = split[0].replace(' ', '');
    const msg = split[1].replace(' ', '');
    console.log(`เบอร์โทรศัพท์ที่ส่ง: ${telephone}\nข้อความที่ส่ง: ${msg}`);
    // const t = new smsClass('twilio');
    const m = new smsClass('username', 'password', 'sender');
    /*  
        You can use function OTP. Example ::: 
            const otp = o.generateOTP();
            const message = `Samsung SMS Code(OTP) is ${otp}`;
    */
    // const sms_t = await t.sendSMS(telephone, 'Twilio');
    const sms_m = await m.sendSMS(telephone, 'Mailbit ' + msg);
    console.log(sms_m);
    process.exit(0)
});