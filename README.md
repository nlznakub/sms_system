# sms_system

## Description
1. เขียนระบบส่งข้อความ SMS
2. ฟังก์ชั่นจะประกอบด้วย 3 ส่วนหลักๆ คือ
  1. API การส่ง sms (ระบบภายนอก)
  2. ระบบ logic
  3. ระบบ storage ที่รองรับ ttl (ไม่ต้องเขียน เพราะปลายทางอาจใช้ database ประเภทไหนก็ไม่รู้)

## Manual Usage / Manual Test
```sh
npm start
Input Phone_number, Message => Ex: +66954856037, Test SMS
```

## API Usage
```js
(async () => {
	const username = `You account id`;
	const password = `You auth token`;
	const sid = ``;
	const o = new smsClass(username, password, sid);
	/**
		* Function generate otp =>  o.generateOTP()
	*/
	const sms = await o.sendSMS(`Phone Number`, `Message`);
})()
```

## WEB MAILBIT
http://sms.mailbit.co.th